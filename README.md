# Fotomoto rW
![Fotomoto rW](./screenshot.png) 

## Description
Fotomoto is a service that allows you to sell prints of your photos through your web site. While Fotomoto does provide an official module ( http://codex.gallery2.org/Gallery3:Modules:fotomoto ) I find that it lacks certain features that I feel are important (like the ability to have their site automatically retrieve high resolution photos for printing) and I have thus made my own module. 

**Security Notice**: This module creates secondary URL's for accessing a photo's resize and fullsize images based on database id. These URL's are publicly viewable, regardless of what security settings may be in place for a photos album. This allows Fotomoto to access them even if the album requires a user to be logged in in order to view it or if the view full size permission is not available to everyone.
For the fullsize image URL I've set up a random string of characters (generated when the module is installed so everyone's is different) to help obscure the location of this URL. This random string of characters can be reset at any time from the module's configuration screen (you'll need to manually update the auto-pickup settings in your Fotomoto account if you do so).
Unfortunately the url for the resize image is viewable in the javascript code on photo pages, preventing me from effectively obscuring it the same way the fullsize url is. *This means anyone could potentially look through resized images, even if they aren't viewable through the Gallery site.*  Keep this in mind if you decide to use this module.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This module installs like any other module -- extract the fotomotorw folder from the attached .zip file and upload it to the gallery/modules directory on your web server. Afterwards log in as an admin and activate it from the modules page (it shows up as "Fotomoto (rWatcher)" to distinguish it from the official module).

Once it's activated, select Admin -> Settings -> Fotomoto to configure the module. You will need to sign up for an account on Fotomoto's web site and enter the site key they give you onto the module's configuration page. The module will then display a few links underneath photos for buying prints and stuff. You can control which links are displayed from the module's configuration page.

If you want to have Fotomoto automatically retrieve high resolution photos when orders are placed, you will need to configure this on their web site. Instructions for doing so are at the bottom of the module's configuration page.

## History
**Version 1.1.0:**
> - Incorporated each photo's upload date/time into the image resize URL to make them harder to guess.
> - Added a "Buy Prints" link to photo thumbnails, under the options menu.
> - Released 11 June 2012.
>
> Download: [Version 1.1.0](/uploads/4cc35d7375fd02dd79aeb240ede0d562/fotomotorw110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 30 May 2012.
>
> Download: [Version 1.0.0](/uploads/7e287c527622cb1e76df4e8b2adc5b7a/fotomotorw100.zip)
